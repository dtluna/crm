Rails.application.routes.draw do
  resources :messages
  resources :orders do
    get '/change_status/:status', to: 'orders#change_status', as: :change_status, on: :member
    resources :messages
  end
  post '/orders/welcome', to: 'welcome#create_order', as: :welcome_orders

  resources :revisions
  resources :worktypes
  resources :parts

  resources :specialities
  devise_for :users, path_names: { sign_up: '/sign_up/:role' }

  scope '/admin' do
    resources :users do
      get '/manage', to: 'users#manage', as: :manage, on: :collection
    end
  end

  scope '/dashboard' do 
    get '', to: 'dashboard#index', as: :dashboard_index
    get 'create_order', to: 'dashboard#create_order', as: :dashboard_create_order
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
