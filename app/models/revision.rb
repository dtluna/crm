class Revision < ActiveRecord::Base
  has_attached_file :document, default_url: false
  do_not_validate_attachment_file_type :document
  validates :comment, presence: true
  validates :order_id, presence: true
  belongs_to :order

  enum status: %i[moderation approved denied]
end
