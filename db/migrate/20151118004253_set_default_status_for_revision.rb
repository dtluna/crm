class SetDefaultStatusForRevision < ActiveRecord::Migration
  def self.up
    add_column :revisions, :status, :integer, default: 0
  end

  def self.down
    # You can't currently remove default values in Rails
    raise ActiveRecord::IrreversibleMigration, "Can't remove the default"
  end
end
